package main.java;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karol on 8.1.2019.
 */
public class StringCalc {
    public static void main(String[] args){
        System.out.println("Hello world!") ;
    }
    public int Add(String numbers) throws StringCalcNegativeException {
        boolean customDelimiterCond = false;
        char customDelimiter = ' ';

        if(numbers.startsWith("//")){
            numbers = numbers.replace("//", "");
            customDelimiter = numbers.charAt(0);
            numbers = numbers.substring(2, numbers.length());
            customDelimiterCond = true;
        }
        if(numbers != ""){
            //unknown amount of characters go through the string and form a list of numbers
            ArrayList<Integer> nums = new ArrayList<Integer>();
            ArrayList<Integer> negatives = new ArrayList<Integer>();
            String currentNumber = "";
            for (int i = 0; i < numbers.length(); i++){
                if (numbers.charAt(i) == ',' || numbers.charAt(i) =='\n' || (customDelimiterCond && numbers.charAt(i) == customDelimiter)){
                    if(currentNumber != "") {
                        nums.add(Integer.parseInt(currentNumber));
                        if(Integer.parseInt(currentNumber) < 0){
                            negatives.add(Integer.parseInt(currentNumber));
                        }
                        currentNumber = "";
                    }
                }else {
                    currentNumber += numbers.charAt(i);
                }
            }

            if(currentNumber != ""){
                nums.add(Integer.parseInt(currentNumber));
                if(Integer.parseInt(currentNumber) < 0){
                    negatives.add(Integer.parseInt(currentNumber));
                }
            }

            if(negatives.size() >0){
                String negativeNumbersString = "";
                for (Integer i: negatives
                     ) {
                    negativeNumbersString += i.toString() + " ";
                }
                throw new StringCalcNegativeException(negativeNumbersString);
            }

            int sum = 0;

            for (Integer i: nums
                 ) {
                sum += i;
            }

            return sum;
        }

        return  0;
    }
}
