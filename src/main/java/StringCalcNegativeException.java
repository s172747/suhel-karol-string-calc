package main.java;

/**
 * Created by karol on 8.1.2019.
 */
public class StringCalcNegativeException extends Exception {
    public StringCalcNegativeException(String message){
        System.out.print("Negative numbers are not allowed!");
        System.out.println("Wrong numbers: " + message);
    }
}
