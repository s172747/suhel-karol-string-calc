package main.java;

import junit.framework.TestCase;
import junit.runner.Version;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.rules.TestWatchman;
import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;

import static org.junit.Assert.*;

/**
 * Created by karol on 8.1.2019.
 */

    public class StringCalcTest extends TestCase {
        StringCalc stringCalc;


        @Rule
        public TestRule watcher = new TestWatcher() {
            protected void starting(Description description) {
                System.out.println("Starting test: " + description.getMethodName());
            }
        };


        @Test
        public void testAddPositive() throws Exception {
            TestCase.assertEquals(0,  stringCalc.Add(""));
            TestCase.assertEquals(1, stringCalc.Add("1"));
            TestCase.assertEquals(3, stringCalc.Add("1,2"));
            TestCase.assertEquals(33, stringCalc.Add("11,22"));
            TestCase.assertEquals(13, stringCalc.Add("11,2"));
            TestCase.assertEquals(23, stringCalc.Add("1,22"));
            System.out.println("JUnit version is: " + Version.id());
        }

        @Test
        public void testAddNegative() {
            try {
                TestCase.assertEquals(-1, stringCalc.Add("-1"));
            } catch (StringCalcNegativeException e) {

            }

        }

        @Test
        public void testUnknown() throws  Exception{
            TestCase.assertEquals(5, stringCalc.Add("1,2,2"));
            TestCase.assertEquals(10, stringCalc.Add("1,2,2,5"));
            TestCase.assertEquals(23, stringCalc.Add("1,2,20"));
            TestCase.assertEquals(19, stringCalc.Add("10,2,2,5"));
        }


        @Test
        public void testUnknownNegative() {
            try {
                TestCase.assertEquals(5, stringCalc.Add("1,2,-2"));
            } catch (StringCalcNegativeException e) {
            }
        }

        @Test
        public void testTwoDelimsBasic() throws  Exception{
            TestCase.assertEquals(3, stringCalc.Add("1\n2"));
            TestCase.assertEquals(11, stringCalc.Add("1\n10"));
        }

        @Test
        public void testTwoDelimsUnknown() throws  Exception{
            TestCase.assertEquals(5, stringCalc.Add("1\n2\n2"));
            TestCase.assertEquals(10, stringCalc.Add("1\n2\n2\n5"));
        }

        @Test
        public void testTwoDelimsNegative(){
            try {
                TestCase.assertEquals(0, stringCalc.Add("1\n2\n2\n-5"));
            } catch (StringCalcNegativeException e) {

            }
        }

        @Test
        public void testTwoDelimsCombined() throws  Exception{
            TestCase.assertEquals(5, stringCalc.Add("1\n2,2"));
            TestCase.assertEquals(10, stringCalc.Add("1\n2,2,5"));
        }

        @Test
        public void testSpecialDelimiterBase() throws  Exception{
            TestCase.assertEquals(3, stringCalc.Add("//;\n1;2"));
            TestCase.assertEquals(6, stringCalc.Add("//l\n1l5"));
        }

        @Test
        public void testSpecialDelimiterUnkown() throws Exception{
            TestCase.assertEquals(5, stringCalc.Add("//;\n1;2;2"));
            TestCase.assertEquals(10, stringCalc.Add("//*\n1*2*2*5"));
        }

        @Test
        public void testSpecialDelimiterCombined() throws  Exception{
            TestCase.assertEquals(5, stringCalc.Add("//;\n1;2,2"));
            TestCase.assertEquals(10, stringCalc.Add("//*\n1\n2,2,5"));
        }


        @Test
        public void testSpecialDelimiterNegative() {
            try {
                TestCase.assertEquals(0, stringCalc.Add("//*\n1\n2,-2,5"));
            } catch (StringCalcNegativeException e) {

            }
        }


        @Before
        public void setUp() throws Exception {
            stringCalc = new StringCalc();
        }

        @After
        public void tearDown() throws Exception {
            //clean up in future
        }

    }
